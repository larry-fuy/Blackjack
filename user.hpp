/**
 *    This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once
#include <string>
#include <iostream>

#include "cards.hpp"
#include "const.hpp"

class User {
public :
   User() : name("User") {};
   User(std::string s) : name(s) {};
   virtual ~User() {};

   inline void set_name(const std::string s) { name = s; };
   inline std::string get_name() const { return name; };

   virtual void clear() { cs.clear(); };

   void add_cards(const Cards cs);

   // get points for all cards in the hand
   int   get_points() const;

   bool is_blackjack() const ;
   inline bool is_bust() const { return (get_points() > 21); }
     
   inline int get_cards_num() const { return cs.size(); }
   Card get_one_card(int i);

   virtual void show_cards();

protected :
   std::string name;
   Cards cs;
};

class Player : public User {
public :  
   Player() : 
      User("Player"), chips(INIT_CHIPS) {};
   Player(std::string s) : 
      User(s), chips(INIT_CHIPS) {};
   virtual ~Player() {};

   inline void set_bet(const int b) { bet = b; }
   inline int    get_bet() const { return bet; }

   inline void set_chips(const int c) { chips = c; }
   inline int    get_chips() const { return chips; }
   inline void add_chips(const int c) { chips += c; }
   inline void reduce_chips(const int c) { chips = (chips - c) > 0 ? chips - c : 0; }
   inline bool is_low_chips() const { return chips < MIN_CHIPS; }

   // ask for soft ace for one card
   void settle_soft_ace(Cards& c);
   // ask for soft aces in all cards in the hand
   void settle_soft_ace();

   virtual void clear() {
      cs.clear();
      set_bet(0);
   }

   void win(user_status s = user_status::STAND);
   void lose();
   void push();
   void show_chips() const;

private :
   int chips;
   int bet;
};

class Dealer : public User {
public :
   Dealer() : User("Dealer") {};
   Dealer(std::string s) : User(s) {};
   virtual ~Dealer() {};

   // one card face down
   virtual void show_cards() const;

   // all cards face up
   virtual void show_all_cards() const;

   // settle soft ace automatically
   void settle_soft_ace();
};
