/**
 *    This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include<vector>
#include<string>

// ascill art banner
const std::vector<std::string> banner = {
  {"  ____  _               _____ _  __    _         _____ _  __"},
  {" |  _ \\| |        /\\   / ____| |/ /   | |  /\\   / ____| |/ / "},
  {" | |_) | |       /  \\ | |    | ' /    | | /  \\ | |    | ' / "},
  {" |  _ <| |      / /\\ \\| |    |  < _   | |/ /\\ \\| |    |  <  "},
  {" | |_) | |____ / ____ \\ |____| . \\ |__| / ____ \\ |____| . \\ "},
  {" |____/|______/_/    \\_\\_____|_|\\_\\____/_/    \\_\\_____|_|\\_\\ "},
  {"  By Y.F."}
 };                                                            
