Blackjack
=========

A simple text-based blackjack game. It is design by the principle KISS (keep it simple and stupid). So there is no third-party library and framework. The constants rather than configuration file are used for behavior of the game.

Build and run
=========

* system requirements
  * Linux kernel 3.12.8, g++ 4.8.2
* make
* make test
* ./blackjack

Description
=========

* One player and one dealer
* User can only be a player since dealer's action is automatically defined by the rules (in real Blackjack, dealer may have some choices, but for simplicity in this game we put restrications on the dealer).
* Start game
  * Enter ./blackjack to start a game. After banner, you'll see
    ```
    Start a new hand? y/n (yes/no) > 
    ```
    Answer y/yes to a new hand.
* Place a bet
  * After you see
    ```	  
    Player please place bet (less than 100) >
    ```
  to place a bet. Be sure the bet not greater than the chips in the hand. 
* Deal
  * After deal you'll see the cards for you and dealer

    	  Dealer Cards : [  ] [S10] 
    	  Player Cards : [C2] [S4]    

    For dealer the first card is always faced down until the  end of this hand. The card"[S10]", for example, means spades 10. "[SA(s)]" means a soft (11 point) spades A. 
* Player's turn
  * You will make several choices to let your cards in good shape after this 
    ```
    Please choose your action : h/s (hit/stand) > 
    ```
    Unless you are bust the next is dealer's turn
* Dealer's turn
  * Dealer will make his choice automatically. So unless the dealer is bust, you'll see

    	   player STAND
    	   dealer STAND
    	   Dealer Cards : [  ] [S10] 
    	   Player Cards : [C2] [S4] 

    And then the result :

    	Player lose
* Quit game
  * After say no to 
    ```
    Start a new hand? y/n (yes/no) > 
    ```
    The you'll see
    ```
    Start a new game? y/n (yes/no) > 
    ```
    Say no again to quit the game.
    

    

Rules 
=========

Based on [Basic rules of Blackjack] (http://www.pagat.com/banking/blackjack.html) and [Wikipedia] (http://en.wikipedia.org/wiki/Blackjack)

  * If player
    * Lose - the player's bet is taken by the dealer.
    * Win - the player wins as much as he bet.
    * Blackjack (natural) - the player wins 1.5 times the bet. 
    * Push - the player keeps his bet, neither winning nor losing money.
  * Blackjack (natural)
    * Only an ace with any ten-point card
  * Bust
    * Player or dealer has cards with total points greater than 21.
  * Win
    * The points of cards is greater than the dealer and less than 21.
    * Dealer is bust.
  * Lose
    * The points of cards is less than the dealer and dealer has no bust.
    * Dealer has Blackjack (natural).
  * Bet
    * Bet before cards dealed
    * No bet is beyond the chips in the hand.
  * Hit and stand
    * Player can choose hit or stand if no Blackjack and bust.
    * Dealer must hit if points is less than 17 and stand if points is 17 or greater.
  * Deck
    * Only 52-card size deck.
    * After each hand the deck is refilled and reshuffled.
    * If no cards for player and dealer, the deck is refilled and reshuffled and then continue.
  * Points
    * The cards 2 through 10 have their face value, J, Q, and K are worth 10 points each. 
    * For player, the Ace is worth either 1 or 11 points by player's choice. If Ace is chosen as 11 points, it is soft.
    * For dealer, the Ace is always 11 points (soft Ace).   
  * Chips
    * Initial chips is 100.
    * The chips in the hand must be greater than 1.   
 
     

To-do list
=========

* Multiple players
* Side rules  (http://www.pagat.com/banking/blackjack.html)
