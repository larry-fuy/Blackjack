/**
 *    This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <cstdlib>

#include "cards.hpp"

int 
Card::get_point() const {
   // handle A
   if (rank == "A") {
      return soft_ace ? SOFT_ACE : HARD_ACE;
   }
  
   // handle other cards
   if (rank == "J" || rank == "Q" || rank == "K") {
      return 10;
   } else {
      int n = stoi(rank);
      if (n <= 10 && n >= 2) {
         return n;
      } else {
         std::cerr << "something wrong in the card : " 
                   << rank << " " << suits << std::endl;
         return -1;
      }
   }
}

