/**
 *    This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <cassert>
#include <algorithm>
#include <cctype>

#include "const.hpp"
#include "user.hpp"

void
User::add_cards(Cards c) {
   if (!c.empty()) {
      for (size_t i = 0; i < c.size(); ++i) 
         cs.push_back(c[i]);
   }
}

void
User::show_cards() {
   std::cout << "Cards : ";
   for (size_t i = 0; i < cs.size(); ++i) 
      std::cout << "[" << cs[i].get_suits() << cs[i].get_rank() << "] ";
   std::cout << std::endl;
}

int
User::get_points() const {
   int sum = 0;
   for (const auto& card : cs)
      sum += card.get_point();
   return sum;
}

bool
User::is_blackjack() const {
      assert(cs.size() == 2);
      std::string first = cs[0].get_rank();
      std::string second = cs[1].get_rank();
      // not use ace_soft to determine since blackjack don't need choice from user
      return (first == "A" && (second  == "10" || second == "J" || second == "Q" || second == "K"))
         || (second == "A" && (first  == "10" || first == "J" || first == "Q" || first == "K"));
}

void 
Player::push() { 
   std::cout << "push... " << std::endl; 
   // take back player's bet
   chips += bet;
}

void
Player::win(user_status s) {
   int bonus = 0;
   if (s == user_status::BLACKJACK) {
      std::cout << get_name() << " win BLACKJACK!" << std::endl;
      bonus = BLACKJACK_BONUS;
   } else if (s == user_status::STAND) {
      std::cout << get_name() << " win!" << std::endl;
      bonus = WIN_BONUS;
   }
   // player's bet and dealer's bet
   chips += bonus * bet + bet;
}

void
Player::lose() {
   std::cout << get_name() << " lose" << std::endl;
}

void
Player::settle_soft_ace() {
   Cards cards = cs;
   settle_soft_ace(cards);
}

void
Player::settle_soft_ace(Cards& cards) {
   for (auto c : cards) {
      if (c.is_ace()) {
         std::cout << "Soft ace (11 point) for " << c.get_suits() << " " << c.get_rank() << ", y/n (yes/no)? > ";
         std::string s;
         std::cin >> s;    
         // find first word
         auto found = find_if(std::begin(s), std::end(s), [](char c) { return std::isspace(c); });
         std::string in = s.substr(0, found - std::begin(s));
         // soft ace is default, so hard ace matters
         if (in == "n" || in == "no") c.set_soft_ace(false); 
      }
   }
}

void
Player::show_chips() const {
   std::cout << "Player chips : " << chips << std::endl;
}

// Dealer always let one ace as soft other as hard. 
// But if he may be bust, all aces are set as hard.
void
Dealer::settle_soft_ace() {
   bool soft_ace_set = false;
   int   soft_ace_pos = 0;
   for (size_t i = 0; i < cs.size(); ++i) {
      if (cs[i].is_ace()) {
         if (cs[i].is_soft_ace() && !soft_ace_set) {
            soft_ace_set = true;
            soft_ace_pos = i;
         } else {
            cs[i].set_soft_ace(false);
         } 
      }
   }
   
   if (is_bust() && soft_ace_set) cs[soft_ace_pos].set_soft_ace(false);
}

void
Dealer::show_cards() const {
   std::cout << "Cards : ";
   std::cout << "[  ] ";
   for (size_t i = 1; i < cs.size(); ++i) {
      if (!cs[i].is_ace()) {
         std::cout << "[" << cs[i].get_suits() << cs[i].get_rank() << "] ";
      } else {
         std::cout << "[" << cs[i].get_suits() << cs[i].get_rank();
         if (cs[i].is_soft_ace()) std::cout << "(s)";
         std::cout << "] ";
      }         
   }
   std::cout << std::endl;  
}

void
Dealer::show_all_cards() const {
   std::cout << "Cards : ";
   for (const auto& card : cs) {
      if (!card.is_ace()) { 
         std::cout << "[" << card.get_suits() << card.get_rank() << "] ";
      } else {
         std::cout << "[" << card.get_suits() << card.get_rank();
         if (card.is_soft_ace())  std::cout  << "(s)";
         std::cout << "] ";
      }
   }
   std::cout << std::endl;  
}
