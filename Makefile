# This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
# 
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

CXX = g++
CXXFLAGS = -std=c++11 -g -Wall
LD = g++
LDFLAGS=

HEADERS=game.hpp user.hpp cards.hpp banner.hpp
SOURCES=user.cpp game.cpp cards.cpp main.cpp
OBJS=$(SOURCES:.cpp=.o)
TARGET=blackjack
TEST_SRC=test_main.cpp user.cpp game.cpp cards.cpp
TEST_OBJS=$(TEST_SRC:.cpp=.o)
TESTS=blackjack_test

.PHONY : all test clean

all : $(TARGET) 

$(TARGET) : $(OBJS) $(HEADERS)
	$(CXX) $(LDFLAGS) $(OBJS) -o $@

.cpp .o : 
	$(CXX) -c $(CXXFLAGS) $< -o $@

test : $(TESTS)
	sh ./runtests.sh

$(TESTS) : $(TEST_OBJS) $(HEADERS)
	$(CXX) $(LDFLAGS) $(TEST_OBJS) -o $@

clean :
	rm -f *~ *.o $(TARGET) $(TESTS) 
