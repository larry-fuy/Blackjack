/**
 *    This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once
#include <vector>
#include <string>

#include "const.hpp"

class Card {
private :
   std::string rank;
   std::string suits;
   // It is a waste for each card to store soft ace info.
   // But it can let us treat all cards in the same way.
   bool soft_ace;

public :
   Card() {};
   Card(std::string r, std::string s, bool sa = true) :
      rank(r), suits(s), soft_ace(sa)
   {}

   // convert point from a card
   int get_point() const;

   inline void set_soft_ace(const bool h) { soft_ace = h; }
   inline bool is_ace() const { return rank == "A"; }
   inline bool is_soft_ace() const { return is_ace() && soft_ace; }  

   inline std::string get_rank() const { return rank; }
   inline void           set_rank(const std::string r) { rank = r; }

   inline std::string get_suits() const { return suits; }
   inline void           set_suits(const std::string s) { suits = s; }
};

typedef std::vector<Card> Cards;
