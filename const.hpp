/**
 *    This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once
// Some constants.
// Todo : use configuration file to set up these constants

const int BLACKJACK_BONUS = 1.5;
const int WIN_BONUS = 1.0;

const int SOFT_ACE = 11;
const int HARD_ACE = 1;

const std::vector<std::string> rank_symbols = {"A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K"};
const std::vector<std::string> suits_symbols = {"H", "S", "D", "C"}; 
const int deck_size = 52;
const int suit_size = 13;

enum class user_status {
   BUST, 
   BLACKJACK,
   STAND
};

const int INIT_CHIPS = 100;
const int MIN_CHIPS  = 1;

const int BUST_POINT = 21;
const int DEALER_STAND_POINT = 17;
