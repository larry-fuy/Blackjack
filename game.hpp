/**
 *    This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "user.hpp"
#include "cards.hpp"

// Coordinator to run the game 
class Game {
public :
   explicit Game(Player* p, Dealer* d) :
      dealer(d), player(p) {};

   void run();
   void load();
   void play();
   void quit();
   user_status player_play();
   user_status dealer_play();

   // deal cards at beginning of each hand
   void initial_deal();

   // refill and shuffle deck
   void shuffle_deck();

   void place_bet();

   // ask for soft ace if any
   void settle_soft_ace();

   // for dealer, one card face down
   void  show_cards();

   // all cards face up
   void  show_all_cards();

   // auxilary functions
   Cards deal_cards(int num = 1);
   void  print_banner() const;
   void  clear();
   inline int get_cards_num() const { return deck.size(); }
   std::string user_input();

private : 
   Dealer* dealer;
   Player* player;
   Cards  deck;
};
