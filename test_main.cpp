/**
 *    This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <vector>
#include <iostream>
#include <string>

#include "user.hpp"
#include "cards.hpp"
#include "game.hpp"

// A simple unit test framework. 
// Since this is a simple project, it is no need 
// to go with full-fledged test frameworks, such 
// as CppUnit or Googletest. And this is also not
// a full-coverage unit tests, only those important
// functions are tested.


void print_message(std::string name, bool pass) {
   std::string s = pass ? "PASSED" : "FAILED";
   std::cout.width(4);
   std::cout << "";
   std::cout.width(20);
   std::cout << std::left << name;
   std::cout.width(20);
   std::cout<< std::left << s << std::endl;
}

// test Player and Dealer
void test_name(Player& player, Dealer& dealer) {
   bool pass = (player.get_name() == "Player" 
                && dealer.get_name() == "Dealer") ? true : false;
   print_message("test_name", pass);   
}

void test_chips(Player& player) {
   bool pass_get = (player.get_chips() == 100);
   player.reduce_chips(50);
   bool pass_reduce = (player.get_chips() == 50);
   player.add_chips(50);
   bool pass_add = (player.get_chips() == 100);
   player.set_chips(0);
   bool pass_low_chips = (player.is_low_chips() == true);
   bool pass = (pass_get && pass_reduce && pass_add && pass_low_chips);
   print_message("test_chips", pass);       
}

void test_bet(Player& player) {
   player.set_bet(100);
   bool pass = (player.get_bet() == 100);
   print_message("test_bet", pass);       
}

void test_clear(Player& player) {
   Cards cards;
   cards.push_back(Card("10", "S"));
   cards.push_back(Card("J", "D"));
   player.add_cards(cards);
   player.clear();
   bool pass = (player.get_cards_num() == 0);
   print_message("test_clear", pass);          
}

void test_cards(Player& player) {
   player.clear();
   Cards cards;
   cards.push_back(Card("2", "S"));
   player.add_cards(cards);
   bool pass_card = (player.get_points() == 2);
   player.clear();
   cards.clear();
   cards.push_back(Card("10", "S"));
   cards.push_back(Card("J", "D"));
   player.add_cards(cards);
   bool pass_cards = (player.get_points() == 20);
   bool pass = pass_card && pass_cards;
   print_message("test_cards", pass);                          
}

void test_is_bust(Player& player) {
   player.clear();
   Cards cards;
   cards.push_back(Card("2", "S"));
   cards.push_back(Card("J", "H"));
   cards.push_back(Card("K", "D"));
   player.add_cards(cards);
   bool pass = (player.is_bust() == true);
   print_message("test_is_bust", pass);                             
}

void test_is_blackjack(Player& player) {
   player.clear();
   Cards cards;
   Card c("A", "S");
   cards.push_back(c);
   cards.push_back(Card("J", "H"));   
   player.add_cards(cards);
   bool pass = (player.is_blackjack() == true);
   print_message("test_is_blackjack", pass);                                
}

void test_user() {
   Player player;
   Dealer dealer;
   std::cout << "test User class: " << std::endl;
   test_name(player, dealer);
   test_chips(player);
   test_bet(player);
   test_cards(player);
   test_clear(player);
   test_is_bust(player);
   test_is_blackjack(player);
}

// test Card
void test_get_point(Card& c) {
   c.set_suits("S");
   c.set_rank("2");
   bool pass_less_ten = (c.get_point() == 2);
   c.set_rank("J");
   bool pass_great_ten = (c.get_point() == 10);
   c.set_rank("A");
   c.set_soft_ace(false);
   bool pass_hard_ace = (c.get_point() == 1);
   c.set_soft_ace(true);
   bool pass_soft_ace = (c.get_point() == 11);
   bool pass = pass_less_ten 
      && pass_great_ten
      && pass_hard_ace
      && pass_soft_ace;
   print_message("test_get_point", pass);   
}

void test_soft_ace(Card& c) {
   c.set_soft_ace(true);
   c.set_rank("A");
   bool pass_is_ace = (c.is_soft_ace() == true);
   c.set_soft_ace(false);
   bool pass_is_not_ace = (c.is_soft_ace() == false);
   bool pass = pass_is_ace && pass_is_not_ace;
   print_message("test_get_ace", pass);   
}
   
void test_card() {
   Card c;
   std::cout << "test Card class: " << std::endl;
   test_soft_ace(c);
   test_get_point(c);
}

// test Game
void test_initial_deal(Game& game, Player* player, Dealer* dealer) {
   game.clear();
   game.initial_deal();
   bool pass = (player->get_cards_num() == 2 
                && dealer->get_cards_num() == 2) ? true : false;
   print_message("test_initial_deal", pass);     
}
  
void test_deal_cards(Game& game) {
   game.clear();
   Cards c1 = game.deal_cards();
   Cards c2 = game.deal_cards(2);
   bool pass = (c1.size() == 1 && c2.size() == 2);
   print_message("test_deal_cards", pass);     
}

void test_shuffle(Game& game) {
   game.clear();
   game.shuffle_deck();
   bool pass = (game.get_cards_num() == deck_size);
   print_message("test_shuffle", pass);        
   // Todo : test randomness of the deck
}

void test_game() {
   Player* player = new Player();
   Dealer* dealer = new Dealer();
   Game game(player, dealer);
   std::cout << "test Game class: " << std::endl;
   game.shuffle_deck();  
   test_initial_deal(game, player, dealer);
   test_deal_cards(game);
   test_shuffle(game);
}

int main() {
   test_user();
   test_card();
   test_game();
}
