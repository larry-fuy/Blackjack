/**
 *    This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <random>
#include <chrono>
#include <iostream>
#include <algorithm>
#include <cstdlib>
#include <cctype>

#include "game.hpp"
#include "banner.hpp"

void
Game::run() {
    load();
    while (true) {
        std::cout << "Start a new game? y/n (yes/no) > ";
        std::string s = user_input();
        if (s == "y" || s == "yes" || s.empty()) {
            load();
        } else if (s == "n" || s == "no") {
            break;
        } else {
            continue;
        }
    }
    quit();
}

void
Game::load() {
    print_banner();
    while (true) {
        std::cout << "Start a new hand? y/n (yes/no) > ";
        std::string s = user_input();
        if (s == "n" || s == "no") {
            break;
        } else if (s == "y" || s == "yes") {
            clear();
            play();
            if (player->is_low_chips()) {
                std::cout << "No enough chips... " << std::endl;  
                break;
            }
        } else {
            continue;
        }
    }
}

// The game is played in 3 stages:
// 1) Deal : if there is blackjack, the hand is over. No 
//      bust in this round.
// 2) Player's turn : player will choose hit or stand if
//     his card is below 21 points. Otherwise he is bust
// 3) Dealer's turn : dealer must hit if his points lower 
//     than 17 points and otherwise stand.
void
Game::play() {
    place_bet();
    initial_deal();
    show_cards();

    // check if there is a natual blackjack
    bool pb = player->is_blackjack();
    bool db = dealer->is_blackjack();
    if (pb && db) {
        player->push();
        show_all_cards();
        player->show_chips();
        return;
    } else if (pb && !db) {
        player->win(user_status::BLACKJACK);
        show_all_cards();
        player->show_chips();
        return;
    } else if (!pb && db) {
        player->lose();
        show_all_cards();
        player->show_chips();
        return;
    } else {
        settle_soft_ace();
    }

    // player's turn
    user_status ps = player_play();
    if (ps == user_status::BUST) {
        player->lose();
        player->show_chips();
        return;
    }

    // dealer's turn
    user_status ds = dealer_play();
    if (ds == user_status::BUST) {
        player->win();
        player->show_chips();
        return;
    }

    // showdown
    if (ds == user_status::STAND
        && ds == user_status::STAND) {
        int dp = dealer->get_points();
        int pp = player->get_points();
        show_all_cards();
        if (dp == pp) {
            player->push();
        } else {
            if (dp > pp)
                player->lose();
            else 
                player->win();
        }
        player->show_chips();
        return;
    }
}

void
Game::quit() {
    std::cout << "Quit game ... " << std::endl;
    delete player;
    delete dealer;
    std::cout << "Goodbye! " << std::endl;
}

// The player can choose hit or stand at will. If no bust,
// player may continue choosing. But the player can 
// choose quit in middle of the game if he is not bust.
user_status
Game::player_play() {
    // player is asked to take action
    while (true)  {
        std::cout << "Please choose your action : h/s (hit/stand) > ";
        std::string s = user_input();
        if (s == "h" || s == "hit") {
            Cards c = deal_cards();
            player->settle_soft_ace(c);
            player->add_cards(c);
            show_cards();
            if (player->is_bust()) {
                std::cout << "you BUST" << std::endl;
                show_all_cards();
                return user_status::BUST;
            }
        } else if (s == "s" || s == "stand") {
            std::cout << "player STAND" << std::endl;
            return user_status::STAND;
        } else {
            std::cout << "please reenter your action" << std::endl;
        }  
    }
}

// the dealer's action is completely dictated by the rules. 
// The dealer must hit if the points is lower than 17, 
// otherwise the dealer will stand. If the points is greater
//  than 21, the dealer is bust. 
user_status
Game::dealer_play() {
    dealer->settle_soft_ace();
    while (true)  {
        int p = dealer->get_points();
        if (p > BUST_POINT) {
            std::cout << "dealer BUST! " << std::endl;
            show_all_cards();
            return user_status::BUST;
        } else if (p >= DEALER_STAND_POINT && p <= BUST_POINT) {
            std::cout << "dealer STAND" << std::endl;
            return user_status::STAND;
        } else {
            std::cout << " dealer HIT " << std::endl;
            dealer->add_cards(deal_cards());
            dealer->settle_soft_ace();
            show_cards();
        }
    }  
}

void
Game::show_cards() {
    std::cout << "Dealer ";
    dealer->show_cards();
    std::cout << "Player ";
    player->show_cards();
}

void
Game::show_all_cards() {
    std::cout << "Dealer ";
    dealer->show_all_cards();
    std::cout << "Player ";
    player->show_cards();
}

// deal two cards to player and dealer
void
Game::initial_deal() {
    player->add_cards(deal_cards(2));
    dealer->add_cards(deal_cards(2));
}

void 
Game::shuffle_deck() {
    std::vector<int> cards_number;
    // generate random numbers between [0 : deck_size]
    for (auto i = 0; i < deck_size; i++) 
        cards_number.push_back(i);
    auto seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::shuffle(std::begin(cards_number), std::end(cards_number), std::default_random_engine(seed));

    // translate the random numbers to cards
    deck.resize(deck_size);
    for (auto i = 0; i < deck_size; ++i) {
        int s = cards_number[i] / suit_size;
        int r = cards_number[i] %  suit_size;
        Card c(rank_symbols[r], suits_symbols[s]);
        deck[i] = c;
    }    
}

Cards
Game::deal_cards(int number) {
    Cards c;
    while (number > 0) {
        if (deck.empty()) shuffle_deck();
        c.push_back(deck.back());
        deck.pop_back();
        number--;
    }
    return c;
}

void 
Game::place_bet() {
    player->show_chips();
    while (true) {
        std::cout << "Player please place bet (less than " 
                  << player->get_chips() <<")" << "> ";
        int b;
        std::cin >> b;
        int c = player->get_chips();
        if (b <= 0) {
            std::cout << "Please place bet more than 1 chips " << std::endl;
            continue;      
        } else if (b >= c) {
            std::cout << "Please place bet less than your chips (" << player->get_chips() << ")" << std::endl;
            continue;
        } else {
            player->set_chips(c - b);
            player->set_bet(b);
            break;
        }
    }
}

void
Game::settle_soft_ace() {
    player->settle_soft_ace();
}

void 
Game::print_banner() const {
    for (std::string s : banner) {
        std::cout << s << std::endl;
    }
    std::cout << std::endl;
}

void
Game::clear() {
    player->clear();
    dealer->clear();
    shuffle_deck();
}

std::string 
Game::user_input() {
    // Todo : hit enter for default action
    std::string in;
    std::cin >> in;    
    if (in.empty()) return in;
    // find first word
    auto found = std::find_if(std::begin(in), std::end(in), [](char c) { return std::isspace(c); });
    std::string ret = in.substr(0, found - std::begin(in));
    return ret;
}
